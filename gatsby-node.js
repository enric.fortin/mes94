/**
 * Implement Gatsby's Node APIs in this file.
 *
 * See: https://www.gatsbyjs.org/docs/node-apis/
 */

// You can delete this file if you're not using it
const _ = require(`lodash`);
const path = require(`path`);
const { slash } = require(`gatsby-core-utils`);

exports.createPages = ({ graphql, actions }) => {
  const { createPage } = actions;

  const policyTemplate = path.resolve(`src/templates/policy.js`);
  return graphql(
    `
      {
        allMarkdownRemark(
          limit: 1000
          filter: { frontmatter: { draft: { ne: true } } }
        ) {
          edges {
            node {
              html
              frontmatter {
                slug
                language
              }
            }
          }
        }
      }
    `
  ).then((result) => {
    if (result.errors) {
      throw result.errors;
    }

    // Create policy pages
    result.data.allMarkdownRemark.edges.forEach((edge) => {
      const { slug } = edge.node.frontmatter;
      createPage({
        path: slug,
        component: slash(policyTemplate),
        context: {
          slug,
        },
      });
    });
  });
};
