const rupture = require("rupture");

const menuLinks = [
  {
    name: "Diseño web",
    link: "/services/web/",
  },
  {
    name: "Diseño gráfico",
    link: "/services/graphic/",
  },
  {
    name: "Marketing",
    link: "/services/marketing/",
  },
  {
    name: "Editorial",
    link: "/services/editorial/",
  },
];

const secondaryMenu = [
  {
    name: "Política de privacidad",
    link: "/privacy/",
  },
  {
    name: "Política de cookies",
    link: "/cookies/",
  },
];

module.exports = {
  siteMetadata: {
    title: `Diseño integral`,
    description: `Estudio de diseño en Huesca. Diseño web, gráfico, marketing y editorial. Soluciones gráficas para problemas cotidianos.`,
    author: `@enfo14`,
    email: "info@estudio-94.com",
    instagram: "estudio94_design",
    menuLinks,
    secondaryMenu,
    siteUrl: "https://estudio-94.com",
  },
  plugins: [
    {
      resolve: `gatsby-plugin-gdpr-cookies`,
      options: {
        googleAnalytics: {
          trackingId: "UA-168808857-1",
          cookieName: "gdpr-google-analytics",
          anonymize: true,
        },
        environments: ["production", "development"],
      },
    },
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: `Estudio 94`,
        short_name: `Estudio 94`,
        start_url: "/",
        display: "minimal-ui",
        icon: "src/images/assets/94-250.png",
      },
    },
    {
      resolve: `gatsby-plugin-stylus`,
      options: {
        use: [rupture()],
      },
    },
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `images`,
        path: `${__dirname}/src/images`,
      },
    },
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `locale`,
        path: `${__dirname}/src/locales`,
      },
    },
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `pages`,
        path: `${__dirname}/src/pages`,
      },
    },
    {
      resolve: `gatsby-plugin-react-i18next`,
      options: {
        localeJsonSourceName: `locale`,
        languages: [`en`, `es`],
        defaultLanguage: `es`,
        siteUrl: `http://localhost:8000`,
        i18nextOptions: {
          interpolation: {
            escapeValue: false,
          },
          keySeparator: false,
          nsSeparator: false,
        },
        pages: [
          {
            matchPath: "/:lang?/cookies/",
            getLanguageFromPath: true,
          },
          {
            matchPath: "/:lang?/privacy/",
            getLanguageFromPath: true,
          },
        ],
      },
    },
    "gatsby-plugin-sitemap",
    "gatsby-transformer-remark",
    "gatsby-plugin-root-import",
    "gatsby-plugin-react-helmet",
    "gatsby-plugin-image",
    "gatsby-plugin-sharp",
    "gatsby-transformer-sharp",
  ],
};
