import React from "react";

import './card-block.styl'

const CardBlock = ({ title, children }) => {
  return (
    <section className="card_block">
      <h2 className="card_block__title">{title}</h2>
      <div className="card_block__cards">
        {children}
      </div>
    </section>
  );
}

export default CardBlock;
