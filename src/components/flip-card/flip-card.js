import React from 'react';
import Img from 'gatsby-image';
import { Link } from 'gatsby';

import './flip-card.styl'

export const FlipCardBlock = ({ children }) => {
  return (
    <div className="flip_card__block">
      {children}
    </div>
  )
}

const StaticCard = ({ image }) => {
  return (
    <div className='static_card'>
      <div className='static_card__inner'>
        <Img fluid={image} />
      </div>
    </div>
  )
}

const DynamicCard = ({ image, text, url }) => {
  return (
    <div className='flip_card'>
      <div className='flip_card__inner'>
        <div className='flip_card__front'>
          <Img fluid={image} />
        </div>
        <div className='flip_card__back'>
          <Link className='flip_card__text' to={url}>{text}</Link>
        </div>
      </div>
    </div>
  )
}

const FlipCard = ({ image, text, url, flip }) => {

  if (flip) {
    return (
      <DynamicCard image={image} text={text} url={url}></DynamicCard>
    )
  } else {
    return (
      <StaticCard image={image} text={text}></StaticCard>
    )
  }

}

export default FlipCard
