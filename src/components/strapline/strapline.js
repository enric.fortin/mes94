import React from "react";

import "./strapline.styl";

const Strapline = ({ text, marker, content, index }) => {
  const getHighlightClass = (i) => {
    const base = "strapline__highlight-inner";

    var previous = index === 0 ? content.length - 1 : index - 1;
    var next = index === content.length - 1 ? 0 : index + 1;

    switch (i) {
      case previous:
        return `${base} --out`;
      case index:
        return `${base} --current`;
      case next:
        return `${base} --in`;
      default:
        return `${base} --hidden`;
    }
  };

  const [left, right] = text.split(marker);
  return (
    <div className="strapline">
      {left && <span className="strapline__text">{left}</span>}
      <span className="strapline__highlight">
        {content.map((item, i) => (
          <span
            key={i}
            className={getHighlightClass(i)}
            style={{ color: item.color }}
          >
            {item.text}
          </span>
        ))}
      </span>
      {right && <span className="strapline__text">{right}</span>}
    </div>
  );
};

export default Strapline;
