import React from 'react';
import PropTypes from 'prop-types';

import './grid.styl'

const Grid = ({ children, title }) => {
  return (
    <div className='grid'>
      <h2 className='grid__title'>{title}</h2>
      <div className='grid__inner'>
        {children}
      </div>
    </div>
  );
};

Grid.propTypes = {
  title: PropTypes.string,
  children: PropTypes.node.isRequired
}

export default Grid;
