import React from 'react';

import './scroll-indicator.styl'

const ScrollIndicator = () => {
  return (
    <div className="scroll-icon">Scroll</div>
  )
}

export default ScrollIndicator
