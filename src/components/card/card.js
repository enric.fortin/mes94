import React, { useEffect, useState } from "react";
import styled from 'styled-components'
import Img from "gatsby-image";

import './card.styl'

const windowGlobal = typeof window !== "undefined" && window;

const CardWrapper = styled.div`
  &:before {
    height: ${props => props.bgHeight}px;
  }
`;

const ImageWrapper = styled.div`
  width: ${props => props.dynamicWidth};
  margin-left: auto;
  margin-right: auto;
`

const Card = ({ title, description, image }) => {
  const titleRef = React.createRef();
  const textRef = React.createRef();
  const [bgHeight, setBgHeight] = useState(0);

  let width = Math.min(image.aspectRatio * 90, 100)
  if (windowGlobal.innerWidth < 880) {
    width = Math.min(image.aspectRatio * 60, 70)
  }

  useEffect(() => {
    const titleStyles = window.getComputedStyle(titleRef.current);
    const textStyles = window.getComputedStyle(textRef.current);
    const titleMarginTop = parseInt(titleStyles.marginTop);
    const titleHeight = titleRef.current.offsetHeight + parseInt(titleStyles.marginTop) + parseInt(titleStyles.marginBottom);
    const textHeight = textRef.current.offsetHeight + parseInt(textStyles.marginTop) + parseInt(textStyles.marginBottom);
    setBgHeight(titleHeight + textHeight + titleMarginTop); // We add titleMarginTop again to fake a margin-bottom
  }, [textRef, titleRef]);

  const img = (
    <Img fluid={image} />
  )

  return (
    <CardWrapper className="card" bgHeight={bgHeight}>
      <div className="card__inner">
        <h2 ref={titleRef} className="card__title">{title}</h2>
        <div ref={textRef} className="card__description" dangerouslySetInnerHTML={{ __html: description }}></div>
      </div>
      <div className="card__image">
        <ImageWrapper dynamicWidth={width}>
          {img}
        </ImageWrapper>
      </div>
    </CardWrapper>
  );
}

export default Card;
