import React, { useEffect, useState } from 'react';
import styled from 'styled-components';
import logo from 'src/images/assets/logo_white.svg';

import SEO from '../seo/seo';
import text from './en.yml'
import './splash.styl'

const OuterContainer = styled.header.attrs(() => {
  let style = {
    opacity: '100%'
  };
  return { style }
})``;

const InnerContainer = styled.div.attrs(({vw, vh}) => {
  let h, w, mx, my;
  if (vh > vw) {
    h = vw * 0.8;
    w = vw * 0.9;
    my = (vh - h) / 2;
    mx = vw * 0.05;
  } else {
    h = vh * 0.8;
    w = vh * 0.9;
    my = vh * 0.1;
    mx = (vw - w) / 2;
  }

  return {
    style: {
      margin: `${my}px ${mx}px`,
      height: `${h}px`,
      width: `${w}px`
    },
  };
})``;

function handleDimensions(setWidth, setHeight) {
  setWidth(window.innerWidth);
  setHeight(window.innerHeight);
  return { width: window.innerWidth, height: window.innerHeight }
}

const Splash = () => {
  const [vw, setWidth] = useState(0);
  const [vh, setHeight] = useState(0);

  const resizeHandler = () => {
    handleDimensions(setWidth, setHeight);
  }

  useEffect(() => {
    handleDimensions(setWidth, setHeight);
    window.addEventListener('resize', resizeHandler);
  }, []);

  return (
    <OuterContainer className="splash">
      <SEO />
      <InnerContainer className="splash__inner" vh={vh} vw={vw}>
        <img className="splash__logo" src={logo} alt="monogram"/>
        <div className="splash__title">
          <h1>{text.header}</h1>
          <h2>{text.subheader}</h2>
        </div>
      </InnerContainer>
    </OuterContainer>
  )
}

export default Splash
