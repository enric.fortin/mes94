---
slug: /en/privacy/
pagename: privacy
language: en
title: "Privacy Policy"
draft: false
manage_cookies: false
---

At Estudio 94, one of our main priorities is the privacy of our visitors. This Privacy Policy document outlines types of information that is collected and recorded by Estudio 94, and how we use it.

If you have additional questions or require more information about our Privacy Policy, do not hesitate to contact us.

This Privacy Policy applies only to our online activities and is validf for visitors to our website with regards to the information that they shared and/or collect in Estudio 94. This policy is not applicable to any information collected via channels other than this website.

#### Consent

By using our website, you hereby consent to our Privacy Policy and agree to its terms.

#### Information we collect

We collect personal information about you, such as your name, email address and phone number, as well as the contents of the message that you may send us, only in case you use our form to contact us directly.

We also gather anonymous information about traffic through our website, including the device and browser you're using, how much time you've spent on site, and the origin of your visit. This information is anonymised the moment it is gathered, and it is never collated with your personal information.

#### How we use your information

We use the information we collect in various ways, including to:

- Send emails specifically in response to contact form submissions
- Understand and analyse how you use our website, for the improvement of performance and user experience
- Operate and maintain our website

#### Log files

Estudio 94 follows a standard procedure of using log files. These files log visitors when they visit the site. All hosting companies do this as part of their hosting services, in order to provide analysis and error debugging.

The information gathered by log files include anonymised internet protocol (IP) addresses, browser type, Internet Service Provider (ISP), date and time stamp, referring/exit pages, and possibly the number of clicks. This information is not linked to any information that is personally identifiable. The purpose of the information is for administration of the site, aggregation of demographic information, and error debugging.

#### Cookies

Like most other websites, Estudio 94 uses 'cookies'. These cookies are used to store information including visitors' preferences, and it is used to optimise the user experience through our website.

For more information, please visit our [Cookie policy](/en/cookies).

#### Third Party Privacy Policies

Estudio 94's Privacy Policy does not apply to other advertisers or websites. Thus, we are advising you to consult the respective Privacy Policies of these third party providers, which may include their practices and instructions about how to manage certain options.

#### General Data Protection Rights (GDPR)

We would like to make sure you are fully aware of all of your data protection rights under GDPR. Every user is entitled to the following:

- Right to access: you have the right to request copies of your personal data.
- Right to rectification: you have the right to request that we correct any information you believe is inaccurate. You also have the right to request that we complete any information you believe is incomplete.
- Right to erasure: you have the right to request that we erase your personal data, under certain conditions.
- Right to object to processing: you have the right to object to our processing of your personal data, under certain conditions.
- Right to data portability: you have the right to request that we transfer your data to another organization, or directly to you, under certain conditions.

If you make a request, we have 30 days to respond to you. If you wish to exercise any of these rights, please get in touch.

#### Children's Information

Another part of our priority is adding protection for children while using the internet. We encourage parents and guardians to observe, participate in, and/or monitor and guide their online activity.

Estudio 94 does not knowingly collect any Personal Identifiable Information from children under the age of 13. If you think that your child provided this kind of information on our website, we strongly encourage you to contact us immediately and we will do our best efforts to promptly remove such information from our records.
