---
slug: /privacy/
pagename: privacy
language: es
title: "Política de privacidad"
draft: false
manage_cookies: false
---

En Estudio 94, la privacidad de nuestros visitantes es una de nuestras prioridades. Este documento de Política de Privacidad indica los tipos de información que Estudio 94 recoge y guarda, y cómo usamos esta información.

Si tienes alguna pregunta o necesitas más información sobre nuestra política de privacidad, no dudes en ponerte en contacto con nosotros.

Esta Política de Privacidad se aplica únicamente a nuestras actividades online a través de esta página, en relación a la información que se obtiene en Estudio 94. Esta política no es aplicable a cualquier información obtenida por otros medios.

#### Consentimiento

Al usar esta página web, consientes a nuestra Política de Privacidad y aceptas estos términos de uso.

#### Información que recogemos

Recogemos información personal sobre ti, como tu nombre, dirección de correo y teléfono, además de el cuerpo del mensaje que nos envíes, únicamente en caso de que utilices nuestro formulario de contacto directamente.

También recogemos información anónima sobre el tráfico en nuestra página web, incluyendo el tipo de dispositivo y navegador que utilizas, cuánto tiempo has pasado en la página, y el origen de tu visita. Esta información es anonimizada en el momento de su captura y no se correlaciona con tu información personal.

#### Cómo usamos tu información

Usamos la información que recogemos de varias maneras, incluyendo:

- Envío de emails específicos en respuesta a mensajes de contacto
- Análisis del uso de la página para el entendimiento y mejora del rendimiento y la experiencia de usuario
- Operación y mantenimiento de la página web

#### Archivos de registro

Estudio 94 sigue un procedimiento estándar de guardado de archivos de registro. Estos archivos registran visitantes a medida que visitan la página web. Todas las compañías de hosting acumulan estos archivos de registro para análisis y depurado de errores.

La información acumulada en estos archivos de registro incluye direcciones anonimizadas de protocolo de internet (IP), tipo de navegador, proveedor de internet (ISP), fecha y hora, páginas de origen/destino, y posiblemente el número de clicks. Esta información no está directamente relacionada con información personalmente identificable. El propósito de esta información es la administración del sitio web, agregación de información demográfica, y depurado de errores.

#### Cookies

Como la mayoría de páginas web, Estudio 94 utiliza 'cookies'. Estas cookies se utilizan para guardar información sobre tus preferencias, y se utilizan para optimizar la experiencia del usuario en su paso por nuestra página web.

Para más información, visita nuestra [política de cookies](/cookies).

#### Política de privacidad de terceros

La Política de Privacidad de Estudio 94 no es aplicable a otras páginas web o servicios digitales. En ese caso, recomendamos que consultes directamente sus Políticas de Privacidad, que pueden indicar sus prácticas e instrucciones de gestión de opciones.

#### Derechos de Protección de Datos (GDPR)

Queremos asegurarnos de que estés informado de todos tus derechos de protección de datos. Todos los usuarios tienen pleno derecho a los siguientes puntos:

- Derecho de acceso: tienes derecho a solicitar copias de tu información personal
- Derecho de rectificación: tienes derecho a solicitar que se corrija cualquier información personal que consideres inexacta. También tienes derecho a solicitar que se complete información que consideres incompleta.
- Derecho de supresión: tienes derecho a solicitar que se elimine toda tu información personal, bajo ciertas circunstancias
- Derecho de oposición: tienes derecho a oponerte al tratamiento de tus datos personales
- Derecho a la portabilidad de los datos: tienes derecho a solicitar que tus datos sean transferidos a otra organización, o directamente a ti, bajo ciertas circunstancias

Si haces alguna solicitud, tenemos 30 días para responder a ella. Si deseas ejercer alguno de estos derechos, por favor, ponte en contacto con nosotros.

#### Información de menores

Otra de nuestras prioridades es asegurar que los menores están totalmente protegidos cuando usan internet. Animamos a los padres y tutores legales a observar, participar, y monitorizar el uso de internet.

Estudio 94 no recoge, con conocimiento de causa, ninguna información personal identificable de personas menores de 13 años. Si crees que tu menor ha proporcionado esta información a nuestra página web, recomendamos encarecidamente que te pongas en contacto con nosotros para que podamos eliminar esta información de nuestros registros.
