---
slug: /cookies/
pagename: cookies
language: es
title: "Política de cookies"
draft: false
manage_cookies: true
---

#### ¿Qué son las cookies?

Esta página web utiliza cookies. Las cookies son ficheros muy pequeños que se descargan a tu navegador para mejorar tu experiencia. Esta página describe qué información agregan, como las usamos y por qué a veces necesitamos hacer uso de ellas.

#### ¿Cómo las usamos?

Utilizamos cookies por una serie de motivos que se indican a continuación. Recomendamos que dejes activadas todas las cookies si no estás seguro de necesitarlos o no, en caso de que se usen para una funcionalidad que utilizas.

#### ¿Cómo puedo desactivarlas?

Nuestras cookies internas son necesarias para el correcto funcionamiento de esta página, por lo que no recomendamos que se desactiven. Por otra parte, al final de esta página puedes activar o desactivar las cookies de análisis según tus preferencias.

#### Nuestras cookies

Utilizamos cookies de preferencias de usuario para recordar tus preferencias de uso de esta página web, de manera que no tenemos que preguntarte tus preferencias cada vez que nos visitas. Esta información está anonimizada y no se comparte nunca con terceros.

#### Cookies de terceros

Utilizamos cookies de terceros para el análisis de tráfico de los visitantes por nuestra página. En particular, este sitio web utiliza Google Analytics, una de las soluciones de análisis de tráfico web más fiables y extendidas, para analizar como utilizas nuestra página y como podemos mejorarla. Estas cookies pueden rastrear cosas como el tiempo que pasas en nuestra web, desde qué tipo de dispositivo y navegador nos visitas, y las páginas que ves. Siempre nos aseguramos que los terceros con los que trabajamos no guarden ningún tipo de información personal, por lo que toda la información acumulada mediante las cookies de Google Analytics es anónima.

Para más información sobre las cookies de Google Analytics, visita la página oficial de Google Analytics.
