---
slug: /en/cookies/
pagename: cookies
language: en
title: "Cookie policy"
draft: false
manage_cookies: true
---

#### What are cookies?

This website uses cookies, which are tiny files that are downloaded to your browser in order to improve your experience. This page describes which information they accumulate, how we use them, and why we need them.

#### How do we use them?

We use cookies for a number of reasons outlined below. We recommend leaving on all cookies if you're unsure whether you need them or not, in case they are required for a feature you use.

#### How can I deactivate them?

Our internal cookies are necessary for the minimal performance of this site, so we do not recommend that they be disabled. On the other hand, at the end of this page you may activate or deactivate the analytics cookies as you wish.

#### Our cookies

We use user preference cookies in order to remember your preferred browsing experience on our site, so that we don't need to ask you every time you visit us. This information is anonymised and never shared with third parties.

#### Third party cookies

We use trusted third party cookies for the analysis of traffic through our site. Specifically, this website uses Google Analytics, one of the most widespread and trusted web analytics solutions, in order to monitor how you use our website and how to improve it. These cookies may track things like the time you spend on the site, which device and browser you're using, and the pages you visit. We always ensure that our third party associates do not store any personal information, so all information gathered by Google Analytics is anonymous.

For more information about Google Analytics cookies, please visit the official Google Analytics website.
